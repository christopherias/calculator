import React from "react"
import Key from "./key"

/* Komponente für Tastenbehälter */
class KeyContainer extends React.Component {
	render() {
		let keyContainerStyle = {
			width: "100vmin",
			height: "80vmin",
		}

		// Farbe für Operatortasten (grau)
		const opKeyColor = "#e6e6e6"

		// Farbe für Zahlentasten (weiß)
		const numKeyColor = "#ffffff" 

		/* Layout der Tasten */
		return(
			<div className="keyContainer" style={keyContainerStyle}> 
				<div className="row pure-g">
					<div className="col pure-u-1-2">
						<Key fct="clear" label="clear" color={opKeyColor} keyPressedCallback={this.props.keyPressedCallback} /> 
					</div>
					<div className="col pure-u-1-2">
						<Key fct="del" label="del" color={opKeyColor} keyPressedCallback={this.props.keyPressedCallback} /> 
					</div>
				</div>

				<div className="row pure-g">
					<div className="col pure-u-1-4">
						<Key fct="7" label="7" color={numKeyColor} keyPressedCallback={this.props.keyPressedCallback} /> 
					</div>
					<div className="col pure-u-1-4">
						<Key fct="8" label="8" color={numKeyColor} keyPressedCallback={this.props.keyPressedCallback} /> 
					</div>
					<div className="col pure-u-1-4">
						<Key fct="9" label="9" color={numKeyColor} keyPressedCallback={this.props.keyPressedCallback} /> 
					</div>
					<div className="col pure-u-1-4">
						<Key fct="division" label="÷" color={opKeyColor} keyPressedCallback={this.props.keyPressedCallback} />
					</div>
				</div>

				<div className="row pure-g">
					<div className="col pure-u-1-4">
						<Key fct="4" label="4" color={numKeyColor} keyPressedCallback={this.props.keyPressedCallback} /> 
					</div>
					<div className="col pure-u-1-4">
						<Key fct="5" label="5" color={numKeyColor} keyPressedCallback={this.props.keyPressedCallback} /> 
					</div>
					<div className="col pure-u-1-4">
						<Key fct="6" label="6" color={numKeyColor} keyPressedCallback={this.props.keyPressedCallback} /> 
					</div>
					<div className="col pure-u-1-4">
						<Key fct="multi" label="x" color={opKeyColor} keyPressedCallback={this.props.keyPressedCallback} />
					</div>
				</div>

				<div className="row pure-g">
					<div className="col pure-u-1-4">
						<Key fct="1" label="1" color={numKeyColor} keyPressedCallback={this.props.keyPressedCallback} /> 
					</div>
					<div className="col pure-u-1-4">
						<Key fct="2" label="2" color={numKeyColor} keyPressedCallback={this.props.keyPressedCallback} /> 
					</div>
					<div className="col pure-u-1-4">
						<Key fct="3" label="3" color={numKeyColor} keyPressedCallback={this.props.keyPressedCallback} /> 
					</div>
					<div className="col pure-u-1-4">
						<Key fct="minus" label="-" color={opKeyColor} keyPressedCallback={this.props.keyPressedCallback} />
					</div>
				</div>

				<div className="row pure-g">
					<div className="col pure-u-1-4">
						<Key fct="leftParens" label="(" color={opKeyColor} keyPressedCallback={this.props.keyPressedCallback} /> 
					</div>
					<div className="col pure-u-1-4">
						<Key fct="0" label="0" color={numKeyColor} keyPressedCallback={this.props.keyPressedCallback} /> 
					</div>
					<div className="col pure-u-1-4">
						<Key fct="rightParens" label=")" color={opKeyColor} keyPressedCallback={this.props.keyPressedCallback} /> 
					</div>
					<div className="col pure-u-1-4">
						<Key fct="plus" label="+" color={opKeyColor} keyPressedCallback={this.props.keyPressedCallback} /> 
					</div>
				</div>

				<div className="row pure-g">
					<div className="col pure-u-3-4">
						<Key fct="equals" label="=" color={opKeyColor} keyPressedCallback={this.props.keyPressedCallback} />
					</div>
					<div className="col pure-u-1-4">
						<Key fct="comma" label="." color={opKeyColor} keyPressedCallback={this.props.keyPressedCallback} /> 
					</div>
				</div>
			</div>
		)
	}
}


export default KeyContainer