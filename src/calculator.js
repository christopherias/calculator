import React from "react"
import Display from "./display"
import KeyContainer from "./keyContainer"

var math = require("mathjs")


/* Komponente für den Taschenrechner (Behälter für alle anderen Komponenten) */
class Calculator extends React.Component {
	constructor() {
		super()

		this.state = {
			userInput : "",
			internalInput: ""
		}
	}

	/* verarbeitet Userinput und sendet Ergebnis an Display */
	buildInputString = (input) => {
		// was der User zu sehen bekommt
		let newInput
		// für die interne Berechnung
		let newInternalInput
		// zur Prüfung, ob Display und Speicher geleert werden sollen
		let clearInput = false
		
		if (input === "plus") {
			newInput = "+"
			newInternalInput = "+"
		} else if (input === "minus") {
			newInput = "-"
			newInternalInput = "-"
		} else if (input === "multi") {
			newInput = "x"
			newInternalInput = "*"
		} else if (input === "division") {
			newInput = "÷"
			newInternalInput = "/"
		} else if (input === "leftParens") {
			newInput = "("
			newInternalInput = "("
		} else if (input === "rightParens") {
			newInput = ")"
			newInternalInput = ")"
		} else if (input === "comma") {
			newInput = "."
			newInternalInput = "."
		} else if (input === "clear") {
			// lösche Usereingabe und leere internen Speicher
			newInput = ""
			newInternalInput = ""
			clearInput = true
		} else if (input === "equals") {
			// ermittle Ergebnis des Userinputs
			let result

			try {
				result = math.eval(this.state.internalInput)
			} catch(err) {
				result = "Error!"
			}

			/* 
				prüfe, ob Ergebnis eine Zahl ist und prüfe anschließend, ob es
				in den MAX-/MIN-Wert passt
			 */
			if (!isNaN(result)) {
				if ( (result <= Number.MAX_SAFE_INTEGER) && (result >= Number.MIN_SAFE_INTEGER)) {
					newInput = result
					newInternalInput = result
				} else {
					newInput = "Number too big!"
					newInternalInput = ""
				}
			} else {
				newInput = "Error!"
				newInternalInput = ""
			}
			clearInput = true
		} else if (input === "del") {
			// lösche Zeichen an letzter Stelle
			if (this.state.userInput.length > 1) {
				newInput = this.state.userInput.substring(0, this.state.userInput.length - 1)
				newInternalInput = this.state.internalInput.substring(0, this.state.internalInput.length - 1)
			} else {
				newInput = ""
				newInternalInput = ""
			}
			clearInput = true
		} else { // Zahleneingabe
			newInput = input
			newInternalInput = input
		}

		if (clearInput === false) {
			this.setState({
				userInput: this.state.userInput + newInput,
				internalInput: this.state.internalInput + newInternalInput
			})
		} else {
			this.setState({
				userInput: newInput,
				internalInput: newInternalInput
			})
		}
	}

	render() {
		/* 
			this.state.userInput get als Prop an Display, sodass es neu gerendert wird,
			wenn sich userInput ändert.
			Die Funktion dafür, buildInputString, wird als Prop an die Komponente Key weitergegeben
			(über die Komponente KeyContainer)
		 */
		return(
			<div className="calculator">
				<Display input={this.state.userInput} />
				<KeyContainer keyPressedCallback={this.buildInputString} />
			</div>
		)
	}
}


export default Calculator