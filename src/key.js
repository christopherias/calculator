import React from "react"

/* Komponente für Tasten */
class Key extends React.Component {
	constructor(props) {
		super(props)

		this.keyPressed = this.keyPressed.bind(this)
	}

	// sendet gedrückten Button an Elternelement-Funktion (buildInputString)
	keyPressed(e) {
		this.props.keyPressedCallback(e.target.dataset.fct)
	}

	render() {
		let keyStyle = {
			width: "100%",
			height: "100%",
			backgroundColor: this.props.color,
			fontSize: "5vmin",
			fontFamily: "monospace",
			textAlign: "center",
			border: "#000000 1px solid"
		}

		return(
			/* Hole Tastenbeschriftung von property "label" */
			<div className="key pure-button" style={keyStyle} onClick={this.keyPressed} data-fct={this.props.fct}>
				{this.props.label}
			</div>
		)
	}
}

export default Key