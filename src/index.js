import React from "react"
import ReactDOM from "react-dom"
import "./index.css"
import Calculator from "./calculator"

/* Rendere Taschenrechner in das DOM */
ReactDOM.render(<Calculator />, document.querySelector("#root"))
