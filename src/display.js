import React from "react"


/* Komponente für das Display */
class Display extends React.Component {
	render() {
		let displayStyle = {
			fontSize: "8vmin",
			fontFamily: "monospace",
			height: "10vmin",
			width: "100vmin",
			border: "#000000 1px solid",
			textAlign: "right",
			verticalAlign: "text-bottom",
			backgroundColor: "#ffffff",
			display: "flex",
			justifyContent: "flex-end",
			alignItems: "center"
		}

		return(
			<div className="display" style={displayStyle}>
				{this.props.input}
			</div>
		)
	}
}

export default Display